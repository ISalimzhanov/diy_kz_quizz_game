package diy.kz_quiz_game

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KzQuizzGameApplication

fun main(args: Array<String>) {
	runApplication<KzQuizzGameApplication>(*args)
}
